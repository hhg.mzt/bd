USE [dsd_mazatlan]
GO
/****** Object:  Table [dbo].[cat_bancos]    Script Date: 27/12/2016 10:25:39 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cat_bancos](
	[id_banco] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](80) NOT NULL,
	[clave_fiscal] [varchar](50) NOT NULL,
	[pagina_web] [varchar](50) NOT NULL,
	[fecha_registro] [date] NOT NULL,
	[usuario_registro] [int] NOT NULL,
	[activo] [bit] NOT NULL,
	[cuenta_contable] [varchar](50) NULL,
 CONSTRAINT [PK_cat_bancos] PRIMARY KEY CLUSTERED 
(
	[id_banco] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[cat_bancos] ADD  CONSTRAINT [DF_cat_bancos_activo]  DEFAULT ((1)) FOR [activo]
GO
