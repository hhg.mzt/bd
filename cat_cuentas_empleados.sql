USE [dsd_mazatlan]
GO
/****** Object:  Table [dbo].[cat_cuentas_empleados]    Script Date: 27/12/2016 10:39:40 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cat_cuentas_empleados](
	[id_cuenta] [int] IDENTITY(1,1) NOT NULL,
	[nro_cuenta] [varchar](21) NOT NULL,
	[id_banco] [int] NOT NULL,
	[fecha_registro] [date] NOT NULL,
	[usuario_registro] [int] NOT NULL,
	[fecha_modifico] [date] NOT NULL,
	[usuario_modifico] [int] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
