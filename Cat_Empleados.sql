USE [dsd_mazatlan]
GO
/****** Object:  Table [dbo].[cat_empleados]    Script Date: 27/12/2016 10:14:04 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
//Comentario de prueba
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cat_empleados](
	[id_empleado] [int] IDENTITY(1,1) NOT NULL,
	[id_area] [int] NULL,
	[id_departamento] [int] NOT NULL,
	[id_puesto] [int] NOT NULL,
	[id_cedis] [int] NOT NULL,
	[clave_contable] [varchar](21) NULL,
	[nombre] [varchar](50) NULL,
	[apellido_paterno] [varchar](50) NULL,
	[apellido_materno] [varchar](50) NULL,
	[calle] [varchar](100) NOT NULL,
	[numero] [varchar](50) NULL,
	[colonia] [varchar](50) NULL,
	[ciudad] [varchar](30) NULL,
	[estado] [varchar](30) NULL,
	[codigo_postal] [int] NULL,
	[correo] [varchar](50) NULL,
	[telefono] [varchar](20) NULL,
	[rfc] [varchar](20) NOT NULL,
	[curp] [varchar](30) NOT NULL,
	[imss] [varchar](20) NOT NULL,
	[tipo_sangre] [varchar](30) NOT NULL,
	[tipo_empleado] [varchar](50) NULL,
	[usuario] [varchar](50) NULL,
	[contrasenia] [varchar](10) NULL,
	[foto] [image] NULL,
	[biometrica] [varbinary](max) NULL,
	[limite_credito] [float] NULL,
	[fecha_registro] [date] NULL,
	[usuario_registro] [int] NULL,
	[activo] [bit] NOT NULL,
	[firma] [varbinary](max) NULL,
	[alta] [int] NOT NULL,
	[entrada_matutina] [varchar](10) NULL,
	[entrada_vespertina] [varchar](10) NULL,
	[clave_contpaq] [varchar](21) NULL,
	[id_fondo_caja] [int] NULL,
	[id_caja_ahorro] [int] NULL,
 CONSTRAINT [PK_cat_empleados_1] PRIMARY KEY CLUSTERED 
(
	[id_empleado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[cat_empleados] ADD  CONSTRAINT [DF_cat_empleados_status]  DEFAULT ((1)) FOR [activo]
GO
ALTER TABLE [dbo].[cat_empleados] ADD  CONSTRAINT [DF__cat_emplea__alta__47B327C1]  DEFAULT ((0)) FOR [alta]
GO
