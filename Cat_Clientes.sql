USE [dsd_mazatlan]
GO
/****** Object:  Table [dbo].[cat_clientes]    Script Date: 27/12/2016 10:13:34 a. m. ******/
SET ANSI_NULLS ON
GO
//cometario de prueba
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cat_clientes](
	[id_cliente] [int] IDENTITY(1,1) NOT NULL,
	[clave_contable] [varchar](21) NULL,
	[matriz] [bit] NOT NULL,
	[id_cliente_matriz] [int] NOT NULL,
	[rfc] [varchar](20) NULL,
	[persona_fisica] [bit] NOT NULL,
	[nombre] [varchar](50) NULL,
	[apellido_paterno] [varchar](50) NULL,
	[apellido_materno] [varchar](50) NULL,
	[razon_social] [varchar](100) NULL,
	[establecimiento] [varchar](100) NOT NULL,
	[calle] [varchar](250) NOT NULL,
	[numero] [varchar](50) NOT NULL,
	[colonia] [varchar](250) NOT NULL,
	[codigo_postal] [varchar](10) NOT NULL,
	[localidad] [varchar](50) NOT NULL,
	[ciudad] [varchar](50) NOT NULL,
	[estado] [varchar](250) NOT NULL,
	[telefono] [varchar](10) NOT NULL,
	[correo] [varchar](50) NULL,
	[fecha_registro] [date] NOT NULL,
	[usuario_registro] [int] NOT NULL,
	[activo] [bit] NOT NULL,
	[metodo_de_pago] [varchar](30) NULL,
	[banco] [varchar](50) NULL,
	[cuenta_bancaria] [varchar](4) NULL,
	[id_cedis_matriz] [int] NOT NULL,
	[fecha_modifico] [date] NULL,
	[usuario_modifico] [int] NULL,
	[mismos_datos] [int] NOT NULL,
	[codigos] [bit] NOT NULL,
	[id_clasificacion] [int] NOT NULL,
	[clave_contpaq] [varchar](21) NULL,
 CONSTRAINT [PK_cat_clientes] PRIMARY KEY CLUSTERED 
(
	[id_cliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[cat_clientes] ADD  CONSTRAINT [DF_cat_clientes_rfc]  DEFAULT ('') FOR [rfc]
GO
ALTER TABLE [dbo].[cat_clientes] ADD  CONSTRAINT [DF_cat_clientes_activo]  DEFAULT ((1)) FOR [activo]
GO
ALTER TABLE [dbo].[cat_clientes] ADD  CONSTRAINT [DF__cat_clien__id_ce__499B7033]  DEFAULT ((0)) FOR [id_cedis_matriz]
GO
ALTER TABLE [dbo].[cat_clientes] ADD  CONSTRAINT [DF__cat_clien__mismo__5D034A62]  DEFAULT ((0)) FOR [mismos_datos]
GO
ALTER TABLE [dbo].[cat_clientes] ADD  CONSTRAINT [DF__cat_clien__codig__3B6D4C6D]  DEFAULT ((0)) FOR [codigos]
GO
ALTER TABLE [dbo].[cat_clientes] ADD  CONSTRAINT [DF_cat_clientes_id_clasificacion]  DEFAULT ((0)) FOR [id_clasificacion]
GO
